﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class Area
    {
        [Required(ErrorMessage = "Este campo es requerido.")]
        public decimal id_Area { get; set; }
        [Required(ErrorMessage = "Este campo es requerido.")]
        public string dsc_Area { get; set; }
    }
}