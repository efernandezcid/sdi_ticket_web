﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Movimiento
    {
        public decimal id_Movimiento { get; set; }
        public DateTime fch_Movimiento { get; set; }
        public decimal id_Solicitud { get; set; }
        public string cod_Estado_Sol { get; set; }
        public decimal id_Usuario { get; set; }

        public DateTime fch_Cod_Estado_Sol { get; set; }

        public string observacion { get; set; }

        public int horas_Dedicadas { get; set; }
        public EstadoSolicitud estado_Movimiento { get; set; }
        public Usuario usuario_Movimiento { get; set; }
        public Solicitud solicitud_Movimiento { get; set; }
        public Documento documento_Movimiento { get; set; }

    }
}