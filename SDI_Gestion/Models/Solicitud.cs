﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class Solicitud
    {
        public decimal id_Solicitud { get; set; }
        public DateTime fch_Ingreso { get; set; }
        [Required(ErrorMessage = "Este campo es requerido.")]
        public string asunto_Sol { get; set; }
        [Required(ErrorMessage = "Este campo es requerido.")]
        public string detalle_Sol { get; set; }
        [Required(ErrorMessage = "Este campo es requerido.")]
        public decimal id_Area { get; set; }
        public decimal id_Usuario { get; set; }
        [Required(ErrorMessage = "Este campo es requerido.")]
        public decimal id_Prioridad { get; set; }
        public string cod_Estado_Sol { get; set; }

        public decimal id_Tipo_Sol { get; set; }

        public DateTime fch_Estado_Sol { get; set; }

        public Usuario usuario_Solicitud { get; set; }

        public Prioridad prioridad_Solicitud { get; set; }

        public EstadoSolicitud estado_Solicitud { get; set; }

        public TipoSolicitud tipo_Solicitud { get; set; }

        public List<Movimiento> movimientos_Solicitud { get; set; }

        public ListasSolicitud ListasSolicitud { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }
    }
}