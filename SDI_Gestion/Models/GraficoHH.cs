﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class GraficoHH
    {
        public decimal id_usuario { get; set; }
        public string nombre_usuario { get; set; }
        public int total_Horas { get; set; }
    }
}