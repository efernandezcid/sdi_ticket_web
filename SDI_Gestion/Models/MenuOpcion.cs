﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class MenuOpcion
    {
       public decimal id_Menu_Opcion { get; set; }
       public string descripcion { get; set; }
       public decimal id_Menu_Padre { get; set; }

       public decimal id_Usuario { get; set; }
    }
}