﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{

    public class Usuario
    {
        public decimal id_Usuario { get; set; }
        public decimal rut_Usuario { get; set; }
        public string dv_Usuario { get; set; }

        [Display(Name = "Rut Usuario")]
        [Required(ErrorMessage = "Este campo es requerido.")]
        [StringLength(12, ErrorMessage = "Longitud máxima 12")]
        public string usuari_RutCompuesto { get; set; }

        [Display(Name = "Nombre Usuario")]
        [Required(ErrorMessage = "Este campo es requerido.")]
        [StringLength(80, ErrorMessage = "Longitud máxima 200")]
        public string nombre_Usr { get; set; }


        [Display(Name = "Apellido Usuario")]
        [Required(ErrorMessage = "Este campo es requerido.")]
        [StringLength(80, ErrorMessage = "Longitud máxima 80")]
        public string ape_Usuario { get; set; }


        [Display(Name = "Correo Electrónico Usuario")]
        [Required(ErrorMessage = "Este campo es requerido.")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",
            ErrorMessage = "Dirección de Correo electrónico incorrecta.")]
        public string email_Usr { get; set; }
        public DateTime fch_Insert { get; set; }
        public decimal id_Area { get; set; }

        public decimal id_Perfil_Usuario { get; set; }

        public string fono_Usuario { get; set; }

        [Display(Name = "Estado Usuario")]
        [Required(ErrorMessage = "Este campo es requerido.")]
        public string cod_Estado_Usr { get; set; }


        public string password { get; set; }


        public Area area_Usuario { get; set; }
        public EstadoUsuario estado_Usuario { get; set; }

        public Perfil perfil_Usuario { get; set; }

    }
}