﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class EstadoSolicitud
    {
        public string cod_Estado_Sol { get; set; }
        public string dsc_Estado_Sol { get; set; } 
    }
}