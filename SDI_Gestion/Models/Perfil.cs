﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class Perfil
    {
        public decimal id_Usuario { get; set; }
        public decimal id_Menu { get; set; }
        public decimal id_Perfil_Usuario { get; set; }
        public string descripcion_Perfil { get; set; }

    }
}