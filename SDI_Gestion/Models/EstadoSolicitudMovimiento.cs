﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class EstadoSolicitudMovimiento
    {
        public decimal id_Solicitud { get; set; }
        public DateTime fch_Movimiento { get; set; }
        public decimal id_Usuario { get; set; }
        [Display(Name = "Observación")]
        [Required(ErrorMessage = "Este campo es requerido.")]
        [StringLength(40, ErrorMessage = "Longitud máxima 40")]
        public string Cod_Estado_Sol { get; set; }
        public int horas_Dedicadas { get; set; }
        public string observacion { get; set; }

    }
}