﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class InformeHH
    { 
        public string area_Descripcion {  get; set; }
        public string nombre_usuario { get; set; }
        public int total_horas { get; set; }
    }
}