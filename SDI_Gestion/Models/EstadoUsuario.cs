﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class EstadoUsuario
    {
        public string cod_Estado_Usr { get; set; }
        public string dsc_Estado_Usr { get; set; }
    }
}