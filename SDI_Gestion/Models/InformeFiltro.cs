﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class InformeFiltro
    {
        public int area_Id { get; set; }
        public int mes { get; set; }
        public int ano { get; set; }

    }
}