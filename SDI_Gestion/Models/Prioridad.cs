﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class Prioridad
    {
        public decimal id_Prioridad { get; set; }
        public string dsc_Prioridad { get; set; }
    }
}