﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SDI_Gestion.Models
{
    public class Documento
    {
        public decimal idDocumento { get; set; }
        public decimal idMovimiento { get; set; }
        public string extencion { get; set; }
        public string evidencia { get; set; }
        public string formato { get; set; }
        public string nombreDocumento { get; set; }

    }
}
