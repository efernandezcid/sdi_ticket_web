﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Models
{
    public class ListasSolicitud
    {
        public List<EstadoSolicitud> EstadosSolicitud { get;set; }
        public List<Prioridad> Prioridades { get; set; }
        public List<Area> Areas { get; set; }
        public List<TipoSolicitud> TiposSolicitud { get; set; }
    }
}