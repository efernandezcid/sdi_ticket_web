﻿using SDI_Gestion.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SDI_Gestion.Utility
{
    public class Mail
    {
        public Task SendEmailAsync(string subject, int formato, Solicitud solicitud, Movimiento movimiento, Usuario usuario )
        {
            try
            {
                string html="";
                //ESTRCTURA DE FORMATO HTML PARA EL CORREO
                //1: Ingreso de nueva solicitud
                if (formato == 1)
                {
                    html = FormatoIngreso(solicitud, usuario); //CUANDO SE INGRESA UNA NUEVA SOLICITUD
                }

                if (formato == 2)
                {
                    html = FormatoSeteoClave(usuario); //CUANDO SE OLVIDA LA CLAVE 
                }


                //GENERICO PARA TODOS LOS CORREOS
                html += "<img src='cid:imagen' width='110' height='110' /><br><br>";
                html += "Este mail es generado de forma automática, favor de no responder.<br>";
                html += "Las tildes han sido omitidas de manera intencional.";

                AlternateView htmlView =
                AlternateView.CreateAlternateViewFromString(html,
                             Encoding.UTF8,
                             MediaTypeNames.Text.Html);

                LinkedResource img =
                //new LinkedResource(@"C:\UNAB\logo.jpeg",
                new LinkedResource(HttpContext.Current.Server.MapPath(("~/img/logo.jpeg")),
                        MediaTypeNames.Image.Jpeg);

                img.ContentId = "imagen";
                htmlView.LinkedResources.Add(img);

                // Credentials
                var credentials = new NetworkCredential("sistem@proyect-ticket.com", "%4Qv95j9y");
                // Mail message
                var mail = new MailMessage()
                {
                    From = new MailAddress("sistem@proyect-ticket.com", "Sistema de Ticket"),
                    Subject = subject, //"TITULO DEL CORREO"
                    //Body = message, //"MENSAJE",
                    IsBodyHtml = true
                };

                mail.AlternateViews.Add(htmlView);

                mail.To.Add(new MailAddress(usuario.email_Usr));

                // Smtp client
                var client = new SmtpClient()
                {
                    Port = 25,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = "mail.proyect-ticket.com",
                    EnableSsl = false,
                    Credentials = credentials
                };

                // Send it...         
                client.Send(mail);
            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }

            return Task.CompletedTask;
        }

        public string FormatoSeteoClave(Usuario usuario)
        {
            //ESTRCTURA DE FORMATO HTML PARA EL CORREO DE SETEO DE CLAVE PROVISORIA
            string html = "<h4>Estimado usuario: " + usuario.nombre_Usr + " " + usuario.ape_Usuario + ", se ha generado una nueva clave provisoria:</h4>" +
                          "<table width='700px' >" +
                          "<tr>" +
                          "<td style='vertical-align:text-top;' align='left' width='150px'>Clave:" +
                          "</td>" +
                          "<td style='vertical-align:text-top;' align='left' width='550px'><b>" + usuario.password + "</b>" +
                          "</td>" +
                          "</tr>" +
                          "</table>";

            return html;
        }

        public string FormatoIngreso(Solicitud solicitud, Usuario usuario)
        {
            //ESTRCTURA DE FORMATO HTML PARA EL CORREO
            string html = "<h4>Estimado usuario: " + usuario.nombre_Usr + " " + usuario.ape_Usuario + ", se ha generado una nueva solicitud de trabajo:</h4>" +
                          "<table width='700px' >" +
                          "<tr>" +
                          "<td style='vertical-align:text-top;' align='left' width='150px'>Numero de solicitud:" +
                          "</td>" +
                          "<td style='vertical-align:text-top;' align='left' width='550px'><b>" + solicitud.id_Solicitud.ToString() + "</b>" +
                          "</td>" +
                          "</tr>" +
                          "<tr>" +
                          "<td style='vertical-align:text-top;' align='left' width='150px'>Asunto Solicitud:" +
                          "</td>" +
                          "<td style='vertical-align:text-top;' align='left'>" + solicitud.asunto_Sol +
                          "</td>" +
                          "</tr>" +
                          "<tr>" +
                          "<td style='vertical-align:text-top;' align='left'>Descripcion:" +
                          "</td>" +
                          "<td style='vertical-align:text-top;' align='left'>" + solicitud.detalle_Sol +
                          "</td>" +
                          "</tr>" +
                          "</table>";

            html += "<br>Para conocer el estado de tu solicitud, favor de ingresar a www.proyect-ticket.com, opcion seguimiento." +
                    "<br>" +
                    "Atte.," +
                    "<br><br>";

            return html;
        }

        public string FormatoMovimiento()
        {
            //ESTRCTURA DE FORMATO HTML PARA EL CORREO
            string html = "<img src='cid:imagen' width='110' height='110' /><br>" +
                          "<h3>Estimado usuario, se ha generado un nuevo movimiento en sistema de requerimintos:</h3>" +


                          "<table width='600' >" +
                          "<tr>" +
                          "<td width='200'>Número de solicitud:" +
                          "</td>" +
                           "<td width='400'>Usuario Solicitante:" +
                          "</td>" +
                          "</tr>" +
                          "</table>";

            return html;
        }
    }
}