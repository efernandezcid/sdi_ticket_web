﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDI_Gestion.Utility
{
    public class Resource
    {
        //public const string APIBaseUrl = "https://api.proyect-ticket.com/";
        public const string APIBaseUrl = "http://localhost:25332/";
        public const string AreaAPIUrl = APIBaseUrl + "api/Area/";
        public const string PerfilesAPIUrl = APIBaseUrl + "api/Perfil";
        public const string PrioridadAPIUrl = APIBaseUrl + "api/Prioridad/";
        public const string SolicitudAPIUrl = APIBaseUrl + "api/Solicitud/";
        public const string TipoSolicitudAPIUrl = APIBaseUrl + "api/TipoSolicitud/";
        public const string TipoSolicitudAreaAPIUrl = APIBaseUrl + "api/TipoSolicitudArea/";
        public const string SolicitudIngAreaAPIUrl = APIBaseUrl + "api/SolicitudIngArea/";
        public const string SolicitudIngUsuarioAPIUrl = APIBaseUrl + "api/SolicitudSeguimiento/";
        public const string MovimientoSolicitudAPIUrl = APIBaseUrl + "api/MovimientoSolicitud/";
        public const string BandejaSolicitudesUsuarioAPIUrl = APIBaseUrl + "api/BandejaSolicitudesUsuario/";
        public const string UsuarioAPIUrl = APIBaseUrl + "api/Usuario/";
        public const string UsuarioLogin = APIBaseUrl + "api/LoginUsuario/"; 
        public const string UsuarioMailAPIUrl = APIBaseUrl + "api/UsuarioMail/";
        public const string UsuarioAreaAPIUrl = APIBaseUrl + "api/UsuarioArea/";
        public const string MovimientoAPIUrl = APIBaseUrl + "api/Movimiento/";
        public const string DocumentoAPIUrl = APIBaseUrl + "api/Documento/";
        public const string GraficoHHAPIUrl = APIBaseUrl + "api/GraficoHH/";
        public const string InformeHHAPIUrl = APIBaseUrl + "api/InformeHH/";
        public const string MenuOpcionDependenciaAPIUrl = APIBaseUrl + "api/MenuOpcionDependencia/";
        public const string MenuOpcionConAccesoXusuarioAPIUrl = APIBaseUrl + "api/MenuConAcceso/";
        public const string MenuOpcionSinAccesoXusuarioAPIUrl = APIBaseUrl + "api/MenuSinAcceso/";
        public const string MenuOpcionusuarioAPIUrl = APIBaseUrl + "api/MenuOpcionUsuario/";

        

    }
}