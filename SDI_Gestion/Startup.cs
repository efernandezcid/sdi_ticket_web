﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(SDI_Gestion.Startup))]

namespace SDI_Gestion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}