﻿using Newtonsoft.Json;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SDI_Gestion.Controllers.Solicitud
{
    public class SeguimientoController : BaseController
    {
        List<Object> myModel = new List<object>();
        // GET: Seguimiento
        // GET: Solicitud
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var httpClient = new HttpClient();
            EstadoSolicitudMovimiento estadoMovimiento = new EstadoSolicitudMovimiento();
            List<SDI_Gestion.Models.Solicitud> solicitudes = new List<SDI_Gestion.Models.Solicitud>();
            List<SDI_Gestion.Models.Solicitud> solicitudesSalida = new List<SDI_Gestion.Models.Solicitud>();
            SDI_Gestion.Models.Solicitud solicitudAsigna = new SDI_Gestion.Models.Solicitud();
            List<Movimiento> movimientos;

            //Rescatamos el usuario que esta conectado
            var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);

            decimal id_Usuario = usuario.id_Usuario;


            //Rescatamos las solicitudes que estan asociadas al area y que estan en estado de ingresadas (ING)
            //Rescatamos los datos de la solicitud
            json = await httpClient.GetStringAsync(Resource.SolicitudIngUsuarioAPIUrl + id_Usuario);
            solicitudes = JsonConvert.DeserializeObject<List<SDI_Gestion.Models.Solicitud>>(json);

            foreach (var sol in solicitudes)
            {
                movimientos = new List<Movimiento>();
                json = await httpClient.GetStringAsync(Resource.MovimientoSolicitudAPIUrl + sol.id_Solicitud);
                movimientos = JsonConvert.DeserializeObject<List<SDI_Gestion.Models.Movimiento>>(json);
                sol.movimientos_Solicitud = movimientos;
                solicitudesSalida.Add(sol);
            }

            myModel.Add(solicitudesSalida);
            myModel.Add(estadoMovimiento);
            myModel.Add(solicitudAsigna);
            ViewBag.Message2 = "Seguimiento";
            return View(myModel);
        }

        [HttpPost]
        public async Task<ActionResult> Cierre(SDI_Gestion.Models.Solicitud solicitud, SDI_Gestion.Models.EstadoSolicitudMovimiento estadoSolicitudMovimiento)
        {
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitudActualizar = new SDI_Gestion.Models.Solicitud();
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;


            if (estadoSolicitudMovimiento.id_Usuario.Equals(0))
            {
                //Rescatamos el usuario que esta conectado
                var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
                Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);
                estadoSolicitudMovimiento.id_Usuario = usuario.id_Usuario;
            }

            var json2 = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + solicitud.id_Solicitud);
            solicitudActualizar = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json2);


            solicitudActualizar.cod_Estado_Sol = "CER";
            solicitudActualizar.fch_Estado_Sol = fechaActual;
            //ACTUALIZAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitudActualizar);


            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimiento.id_Solicitud = solicitud.id_Solicitud;
            estadoSolicitudMovimiento.horas_Dedicadas = 0;
            estadoSolicitudMovimiento.fch_Movimiento = fechaActual;
            estadoSolicitudMovimiento.Cod_Estado_Sol = "CER";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimiento);

            //TempData["Mensaje"] = "Asigación de solicitud correcta";

            return RedirectToAction("Index", "Seguimiento");

        }

        [HttpPost]
        public async Task<ActionResult> Rechazo(SDI_Gestion.Models.Solicitud solicitudRechazo, SDI_Gestion.Models.EstadoSolicitudMovimiento estadoSolicitudMovimiento, SDI_Gestion.Models.Archivo archivo)
        {
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitudActualizar = new SDI_Gestion.Models.Solicitud();
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;


            if (estadoSolicitudMovimiento.id_Usuario.Equals(0))
            {
                //Rescatamos el usuario que esta conectado
                var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
                Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);
                estadoSolicitudMovimiento.id_Usuario = usuario.id_Usuario;
            }

            var json2 = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + solicitudRechazo.id_Solicitud);
            solicitudActualizar = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json2);

            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimiento.horas_Dedicadas = 0;
            estadoSolicitudMovimiento.id_Solicitud = solicitudRechazo.id_Solicitud;
            estadoSolicitudMovimiento.fch_Movimiento = fechaActual;
            estadoSolicitudMovimiento.Cod_Estado_Sol = "OBS";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimiento);


            ///CODIGO PARA GENERAR ESTADO DE DEVOLUCION************************************
            solicitudActualizar.cod_Estado_Sol = "DEV";
            solicitudActualizar.fch_Estado_Sol = fechaActual;
            //ACTUALIZAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitudActualizar);


            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimiento.horas_Dedicadas = 0;
            estadoSolicitudMovimiento.id_Solicitud = solicitudRechazo.id_Solicitud;
            estadoSolicitudMovimiento.id_Usuario = solicitudRechazo.id_Usuario;
            estadoSolicitudMovimiento.fch_Movimiento = fechaActual;
            estadoSolicitudMovimiento.Cod_Estado_Sol = "DEV";
            estadoSolicitudMovimiento.observacion = "Usuario solicitante rechazó la resolución. Consulta el historial de solicitud para saber el motivo.";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimiento);

            Movimiento movimientoGenerado = JsonConvert.DeserializeObject<SDI_Gestion.Models.Movimiento>(await respuesta.Content.ReadAsStringAsync());

            if (!(archivo.ImageFile == null))
            {
                //Guarda el documento en la BBDD
                Documento documento = new Documento();
                string evidencia;
                string extension = Path.GetExtension(archivo.ImageFile.FileName);
                string formato = (archivo.ImageFile.ContentType);
                string nombreArchivo = Path.GetFileNameWithoutExtension(archivo.ImageFile.FileName);
                using (var binaryreader = new BinaryReader(archivo.ImageFile.InputStream))
                {
                    evidencia = Convert.ToBase64String(binaryreader.ReadBytes(archivo.ImageFile.ContentLength));
                }
                //Enviamos el documento
                documento.idMovimiento = movimientoGenerado.id_Movimiento;
                documento.extencion = extension;
                documento.evidencia = evidencia;
                documento.formato = formato;
                documento.nombreDocumento = nombreArchivo;
                respuesta = await httpClient.PostAsJsonAsync(Resource.DocumentoAPIUrl, documento);

            }

            return RedirectToAction("Index", "Seguimiento");

        }
        
        [HttpGet]
        public async Task<ActionResult> Download(int idDocumento)
        {
            var httpClient = new HttpClient();

            var respuesta = await httpClient.GetAsync($"{Resource.DocumentoAPIUrl}{idDocumento}" ); //va a buscar el documento con el idMov
            var json  = await respuesta.Content.ReadAsStringAsync();
            var d = JsonConvert.DeserializeObject<Documento>(json);

            JsonResult jResult = Json(d, JsonRequestBehavior.AllowGet );
            jResult.MaxJsonLength = int.MaxValue;

            return jResult;
        }
    }
}