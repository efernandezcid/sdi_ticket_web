﻿using Newtonsoft.Json;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SDI_Gestion.Controllers.Solicitud
{
    public class SolicitudController : BaseController
    {
        List<Object> myModel = new List<object>();
        Usuario usuarioMail = new Usuario();

        // GET: Solicitud
        public async Task<ActionResult> Index()
        {
            //LLAMADAS A LA API
            List<TipoSolicitud> tiposSolicitud = new List<TipoSolicitud>();

            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitud = new SDI_Gestion.Models.Solicitud();

            //rescatamos las areas
            var json = await httpClient.GetStringAsync(Resource.AreaAPIUrl);
            List<Area> areas = JsonConvert.DeserializeObject<List<Area>>(json);

            //rescatamos las prioridades 
            json = await httpClient.GetStringAsync(Resource.PrioridadAPIUrl);
            List<Prioridad> prioridades = JsonConvert.DeserializeObject<List<Prioridad>>(json);

            //Rescatamos el usuario que esta conectado
            json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);

            solicitud.usuario_Solicitud = usuario;
            solicitud.ListasSolicitud = new ListasSolicitud();
            solicitud.ListasSolicitud.Areas = areas;
            solicitud.ListasSolicitud.Prioridades = prioridades;
            solicitud.ListasSolicitud.TiposSolicitud = tiposSolicitud;


            myModel.Add(solicitud);
            ViewBag.Message2 = "Ingreso";
            return View(myModel);
        }

        [HttpPost]
        public Task<string> BuscarTiposSolicitudArea(int idArea)
        {
            var httpClient = new HttpClient();
            var tiposSolicitud = httpClient.GetStringAsync(Resource.TipoSolicitudAreaAPIUrl + idArea);
            return tiposSolicitud;
        }

        // GET: Solicitud
        [HttpGet]
        public async Task<ActionResult> CargarSolicitud(decimal id_Solicitud)
        {
            //LLAMADAS A LA API
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitud = new SDI_Gestion.Models.Solicitud();

            //rescatamos las areas
            var json = await httpClient.GetStringAsync(Resource.AreaAPIUrl);
            List<Area> areas = JsonConvert.DeserializeObject<List<Area>>(json);

            //rescatamos las prioridades 
            json = await httpClient.GetStringAsync(Resource.PrioridadAPIUrl);
            List<Prioridad> prioridades = JsonConvert.DeserializeObject<List<Prioridad>>(json);

            //Rescatamos los datos de la solicitud
            json = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + id_Solicitud);
            solicitud = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json);

            //Cargamos tipos de solicitud degun el área que tiene la solicitud
            json = await httpClient.GetStringAsync(Resource.TipoSolicitudAreaAPIUrl + solicitud.id_Area);
            List<TipoSolicitud> tiposSolicitud = JsonConvert.DeserializeObject<List<TipoSolicitud>>(json);


            //Rescatamos el usuario que esta conectado
            json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);

            solicitud.usuario_Solicitud = usuario;
            solicitud.ListasSolicitud = new ListasSolicitud();
            solicitud.ListasSolicitud.Areas = areas;
            solicitud.ListasSolicitud.Prioridades = prioridades;
            solicitud.ListasSolicitud.TiposSolicitud = tiposSolicitud;

            myModel.Add(solicitud);

            return View(myModel);
        }

        // GET: Solicitud
        [HttpGet]
        public async Task<ActionResult> Toma()
        {
            var httpClient = new HttpClient();
            EstadoSolicitudMovimiento estadoMovimiento = new EstadoSolicitudMovimiento();
            List<SDI_Gestion.Models.Solicitud> solicitudes = new List<SDI_Gestion.Models.Solicitud>();
            SDI_Gestion.Models.Solicitud solicitudAsigna = new SDI_Gestion.Models.Solicitud();

            //Rescatamos el usuario que esta conectado
            var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);

            decimal id_Area = usuario.id_Area;


            //Rescatamos las solicitudes que estan asociadas al area y que estan en estado de ingresadas (ING)
            //Rescatamos los datos de la solicitud
            json = await httpClient.GetStringAsync(Resource.SolicitudIngAreaAPIUrl + id_Area);
            solicitudes = JsonConvert.DeserializeObject<List<SDI_Gestion.Models.Solicitud>>(json);

            myModel.Add(solicitudes);
            myModel.Add(estadoMovimiento);
            myModel.Add(solicitudAsigna);
            ViewBag.Message2 = "Tomar";
            return View(myModel);
        }

        // GET: Solicitud
        [HttpGet]
        public async Task<ActionResult> Asignacion()
        {
            var httpClient = new HttpClient();
            EstadoSolicitudMovimiento estadoMovimiento = new EstadoSolicitudMovimiento();
            List<SDI_Gestion.Models.Solicitud> solicitudes = new List<SDI_Gestion.Models.Solicitud>();
            List<SDI_Gestion.Models.Usuario> usuarios = new List<SDI_Gestion.Models.Usuario>();
            SDI_Gestion.Models.Solicitud solicitudAsigna = new SDI_Gestion.Models.Solicitud();

            //Rescatamos el usuario que esta conectado
            var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);

            decimal id_Area = usuario.id_Area;


            //Rescatamos las solicitudes que estan asociadas al area y que estan en estado de ingresadas (ING)
            //Rescatamos los datos de la solicitud
            json = await httpClient.GetStringAsync(Resource.SolicitudIngAreaAPIUrl + id_Area);
            solicitudes = JsonConvert.DeserializeObject<List<SDI_Gestion.Models.Solicitud>>(json);


            //Rescatamos los usuarios del área del perfil conectado
            json = await httpClient.GetStringAsync(Resource.UsuarioAreaAPIUrl + id_Area);
            usuarios = JsonConvert.DeserializeObject<List<SDI_Gestion.Models.Usuario>>(json);


            myModel.Add(solicitudes);
            myModel.Add(estadoMovimiento);
            myModel.Add(solicitudAsigna);
            myModel.Add(usuarios);
            ViewBag.Message2 = "Asignar";
            return View(myModel);
        }

        [HttpPost]
        public async Task<ActionResult> Ingreso(SDI_Gestion.Models.Solicitud solicitud, SDI_Gestion.Models.Archivo archivo)
        {
            var httpClient = new HttpClient();
            HttpStatusCode codigo;
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;

            Movimiento movimiento = new Movimiento();

            //Datos Solicitud
            solicitud.fch_Ingreso = fechaActual;
            solicitud.fch_Estado_Sol = fechaActual;
            solicitud.id_Usuario = solicitud.usuario_Solicitud.id_Usuario;
            solicitud.cod_Estado_Sol = "ING";
            //GENERAMOS LA SOLCITUD
            respuesta = await httpClient.PostAsJsonAsync(Resource.SolicitudAPIUrl, solicitud);
            codigo = respuesta.StatusCode;
            Uri urlCotizacion = respuesta.Headers.Location;

            var json = await httpClient.GetStringAsync(urlCotizacion);
            SDI_Gestion.Models.Solicitud solicitudSalida = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json);

            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            movimiento.id_Solicitud = solicitudSalida.id_Solicitud;
            movimiento.id_Usuario = solicitud.id_Usuario;
            movimiento.fch_Movimiento = fechaActual;
            movimiento.horas_Dedicadas = 0;
            movimiento.cod_Estado_Sol = "ING";
            movimiento.observacion = "Ingreso de Solicitud";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, movimiento);
            codigo = respuesta.StatusCode;

            Movimiento movimientoGenerado = JsonConvert.DeserializeObject<SDI_Gestion.Models.Movimiento>(await respuesta.Content.ReadAsStringAsync());

            //Enviamos el documento
            if (!(archivo.ImageFile == null))
            {
                Documento documento = new Documento();

                string evidencia;
                string extension = Path.GetExtension(archivo.ImageFile.FileName);
                string formato = (archivo.ImageFile.ContentType);
                string nombreArchivo = Path.GetFileNameWithoutExtension(archivo.ImageFile.FileName);
                using (var binaryreader = new BinaryReader(archivo.ImageFile.InputStream))
                {
                    evidencia = Convert.ToBase64String(binaryreader.ReadBytes(archivo.ImageFile.ContentLength));
                }
                documento.idMovimiento = movimientoGenerado.id_Movimiento;
                documento.extencion = extension;
                documento.evidencia = evidencia;
                documento.formato = formato;
                documento.nombreDocumento = nombreArchivo;
                respuesta = await httpClient.PostAsJsonAsync(Resource.DocumentoAPIUrl, documento);
                codigo = respuesta.StatusCode;
            }
            
            usuarioMail.nombre_Usr = Session["user_Name"].ToString();
            usuarioMail.ape_Usuario = Session["user_Apellido"].ToString();
            usuarioMail.email_Usr = Session["user_Email"].ToString();
            Utility.Mail mail = new Mail();
            Task respuestaMail = mail.SendEmailAsync("Ingreso de solicitud", 1, solicitudSalida, null, usuarioMail);

            TempData["Mensaje"] = "Solicitud creada correctamente.";

            return RedirectToAction("CargarSolicitud", "Solicitud", routeValues: new { id_Solicitud = solicitudSalida.id_Solicitud });

        }

        [HttpPost]
        public async Task<ActionResult> Modifica(SDI_Gestion.Models.Solicitud solicitud)
        {
            var httpClient = new HttpClient();
            HttpResponseMessage respuesta;
            Movimiento movimiento = new Movimiento();
            solicitud.cod_Estado_Sol = "ING";
            //GENERAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitud);

            TempData["Mensaje"] = "Solicitud modificada correctamente.";

            return RedirectToAction("CargarSolicitud", "Solicitud", routeValues: new { id_Solicitud = solicitud.id_Solicitud });

        }

        [HttpPost]
        public async Task<ActionResult> Asignacion(SDI_Gestion.Models.Solicitud solicitud, SDI_Gestion.Models.EstadoSolicitudMovimiento estadoSolicitudMovimiento)
        {
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitudActualizar = new SDI_Gestion.Models.Solicitud();
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;


            if (estadoSolicitudMovimiento.id_Usuario.Equals(0))
            {
                //Rescatamos el usuario que esta conectado
                var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
                Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);
                estadoSolicitudMovimiento.id_Usuario = usuario.id_Usuario;
            }

            var json2 = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + solicitud.id_Solicitud);
            solicitudActualizar = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json2);

            solicitudActualizar.cod_Estado_Sol = "ASI";
            solicitudActualizar.fch_Estado_Sol = fechaActual;
            //ACTUALIZAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitudActualizar);

            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimiento.id_Solicitud = solicitud.id_Solicitud;
            estadoSolicitudMovimiento.horas_Dedicadas = 0;
            estadoSolicitudMovimiento.fch_Movimiento = fechaActual;
            estadoSolicitudMovimiento.Cod_Estado_Sol = "ASI";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimiento);

            TempData["Mensaje"] = "Asigación de solicitud correcta";

            return RedirectToAction("Toma", "Solicitud");

        }

    }
}