﻿using Newtonsoft.Json;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SDI_Gestion.Controllers.Mantenedores
{
    public class UsuarioController : BaseController
    {
        List<Object> myModel = new List<object>();

        // GET: Usuario
        [HttpGet]
        public async Task<ActionResult> Index()
        {


            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync(Resource.UsuarioAPIUrl);
            List<Usuario> usuarioList = JsonConvert.DeserializeObject<List<Usuario>>(json);

            json = await httpClient.GetStringAsync(Resource.AreaAPIUrl);
            List<Area> areaList = JsonConvert.DeserializeObject<List<Area>>(json);

            json = await httpClient.GetStringAsync(Resource.PerfilesAPIUrl);
            List<Perfil> perfilList = JsonConvert.DeserializeObject<List<Perfil>>(json);

            Usuario userEdit = new Usuario();
            Usuario userNew = new Usuario();

            myModel.Add(usuarioList);
            myModel.Add(userEdit);
            myModel.Add(userNew);
            myModel.Add(perfilList);
            myModel.Add(areaList);

            //ViewBag.Message = "Mantenedores";
            ViewBag.Message2 = "Usuarios";
            return View(myModel);

        }

        [HttpPost]
        public async Task<ActionResult> EditaModifica(Usuario usuarioEdit, Usuario usuarioNew)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response;
            Usuario usuarioEnvia = new Usuario();
            

            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);


            if (usuarioEdit.id_Usuario > 0)
            {
                string[] arreg_rut = usuarioEdit.usuari_RutCompuesto.ToString().Replace(".", "").Split('-');
                usuarioEnvia.id_Usuario = usuarioEdit.id_Usuario;
                usuarioEnvia.rut_Usuario = Convert.ToInt32(arreg_rut[0]);
                usuarioEnvia.dv_Usuario = arreg_rut[1];
                usuarioEnvia.nombre_Usr = usuarioEdit.nombre_Usr;
                usuarioEnvia.ape_Usuario = usuarioEdit.ape_Usuario;
                usuarioEnvia.email_Usr = usuarioEdit.email_Usr;
                usuarioEnvia.fono_Usuario = usuarioEdit.fono_Usuario;
                usuarioEnvia.cod_Estado_Usr = usuarioEdit.cod_Estado_Usr;
                usuarioEnvia.id_Area = usuarioEdit.area_Usuario.id_Area;
                usuarioEnvia.id_Perfil_Usuario = usuarioEdit.perfil_Usuario.id_Perfil_Usuario;
                usuarioEnvia.fch_Insert = fechaActual;
                response = await client.PutAsJsonAsync(Resource.UsuarioAPIUrl, usuarioEnvia);
                TempData["Mensaje"] = "Usuario modificado correctamente.";
            }
            else
            {
                string[] arreg_rut = usuarioNew.usuari_RutCompuesto.ToString().Replace(".", "").Split('-');
                usuarioEnvia.id_Usuario = 0;
                usuarioEnvia.rut_Usuario = Convert.ToInt32(arreg_rut[0]);
                usuarioEnvia.dv_Usuario = arreg_rut[1];
                usuarioEnvia.nombre_Usr = usuarioNew.nombre_Usr;
                usuarioEnvia.ape_Usuario = usuarioNew.ape_Usuario;
                usuarioEnvia.email_Usr = usuarioNew.email_Usr;
                usuarioEnvia.fono_Usuario = usuarioNew.fono_Usuario;
                usuarioEnvia.cod_Estado_Usr = "PEN";
                usuarioEnvia.id_Area = usuarioNew.area_Usuario.id_Area;
                usuarioEnvia.id_Perfil_Usuario = usuarioNew.perfil_Usuario.id_Perfil_Usuario;
                usuarioEnvia.fch_Insert = fechaActual;
                usuarioEnvia.password = "12345678";
                response = await client.PostAsJsonAsync(Resource.UsuarioAPIUrl, usuarioEnvia);
                TempData["Mensaje"] = "Usuario creado correctamente.";
            }

            

            ViewBag.Message = "Mantenedores";
            ViewBag.Message2 = "Usuarios";
            return RedirectToAction("Index", "Usuario");

        }
    }
}