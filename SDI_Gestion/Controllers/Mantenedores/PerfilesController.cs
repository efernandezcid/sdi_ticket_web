﻿using Newtonsoft.Json;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SDI_Gestion.Controllers.Mantenedores
{
    public class PerfilesController : BaseController
    {
        List<Object> myModel = new List<object>();

        // GET: Perfiles
        public async Task<ActionResult> Index()
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync(Resource.UsuarioAPIUrl);
            List<Usuario> usuarioList = JsonConvert.DeserializeObject<List<Usuario>>(json);

            json = await httpClient.GetStringAsync(Resource.MenuOpcionDependenciaAPIUrl + "1");
            List<MenuOpcion> menusPrincipales = JsonConvert.DeserializeObject<List<MenuOpcion>>(json);

            myModel.Add(usuarioList);
            myModel.Add(menusPrincipales);
            return View(myModel);
        }

        public async Task<ActionResult> Cargar(Perfil datosSeleccion)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync(Resource.UsuarioAPIUrl);
            List<Usuario> usuarioList = JsonConvert.DeserializeObject<List<Usuario>>(json);

            json = await httpClient.GetStringAsync(Resource.MenuOpcionDependenciaAPIUrl + "1");
            List<MenuOpcion> menusPrincipales = JsonConvert.DeserializeObject<List<MenuOpcion>>(json);

            json = await httpClient.GetStringAsync(Resource.MenuOpcionConAccesoXusuarioAPIUrl + datosSeleccion.id_Menu + "/" + datosSeleccion.id_Usuario);
            List<MenuOpcion> menusConAcceso = JsonConvert.DeserializeObject<List<MenuOpcion>>(json);

            json = await httpClient.GetStringAsync(Resource.MenuOpcionSinAccesoXusuarioAPIUrl + datosSeleccion.id_Menu + "/" + datosSeleccion.id_Usuario);
            List<MenuOpcion> menusSinAcceso = JsonConvert.DeserializeObject<List<MenuOpcion>>(json);

            myModel.Add(datosSeleccion);
            myModel.Add(usuarioList);
            myModel.Add(menusPrincipales);
            myModel.Add(menusConAcceso);
            myModel.Add(menusSinAcceso);
            return View(myModel);
        }

        [HttpPost]
        public async Task<ActionResult> Agregar(MenuOpcion opcionAgrega)
        {
            HttpResponseMessage response;
            Perfil datosSeleccion = new Perfil();
            datosSeleccion.id_Menu = opcionAgrega.id_Menu_Padre;
            datosSeleccion.id_Usuario = opcionAgrega.id_Usuario;

            MenuOpcionUsuario menuOpcionUsuario = new MenuOpcionUsuario();
            menuOpcionUsuario.id_Menu_Opcion = opcionAgrega.id_Menu_Opcion;
            menuOpcionUsuario.id_Usuario = opcionAgrega.id_Usuario;

            var httpClient = new HttpClient();
            response =  await httpClient.PostAsJsonAsync(Resource.MenuOpcionusuarioAPIUrl, menuOpcionUsuario);
         

            return RedirectToAction("Cargar", "Perfiles", datosSeleccion);

        }

        [HttpPost]
        public async Task<ActionResult> Eliminar(MenuOpcion opcionElimina)
        {
            HttpResponseMessage response;
            Perfil datosSeleccion = new Perfil();
            datosSeleccion.id_Menu = opcionElimina.id_Menu_Padre;
            datosSeleccion.id_Usuario = opcionElimina.id_Usuario;

            MenuOpcionUsuario menuOpcionUsuario = new MenuOpcionUsuario();
            menuOpcionUsuario.id_Menu_Opcion = opcionElimina.id_Menu_Opcion;
            menuOpcionUsuario.id_Usuario = opcionElimina.id_Usuario;

            var httpClient = new HttpClient();
            response = await httpClient.PutAsJsonAsync(Resource.MenuOpcionusuarioAPIUrl, menuOpcionUsuario);

            return RedirectToAction("Cargar", "Perfiles", datosSeleccion);
            //return View(myModel);
        }

    }
}