﻿using Newtonsoft.Json;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SDI_Gestion.Controllers.Mantenedores
{
    public class CambioClaveController : Controller
    {
        List<Object> myModel = new List<object>();
        Usuario usuarioMail = new Usuario();

        // GET: CambioClave
        public ActionResult Index()
        {
            if (!(Session["user_Id"] == null) && Session["user_Id"].ToString() != "")
            {
                Session["cambio_clave"] = "S";
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        // GET: CambioClave
        public ActionResult Olvido(string email_usuario)
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> OlvidoSetea(string email_usuario)
        {
            var httpClient = new HttpClient();
            HttpResponseMessage response;
            Usuario usuario = new Usuario();
            Usuario usuarioEnvia = new Usuario();
            var json="";

            //Rescatamos el usuario que esta conectado
            try
            {
                json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + email_usuario);
            }
            catch (Exception ex) 
            {
                TempData["Mensaje"] = ex.Source; 
            }
            usuario = JsonConvert.DeserializeObject<Usuario>(json);

            if (usuario == null)
            {
                TempData["Cod"] = "NO-OK";
                TempData["Mensaje"] = "usuario ingresado no existe.";
                return RedirectToAction("Olvido", "OlvidoClave");
            }
            else
            {
                usuarioEnvia.id_Usuario = usuario.id_Usuario;
                usuarioEnvia.cod_Estado_Usr = "PEN";
                usuarioEnvia.password = generaClaveProvisoria();
                response = await httpClient.PutAsJsonAsync(Resource.UsuarioAPIUrl, usuarioEnvia);

                usuarioMail.nombre_Usr = usuario.nombre_Usr;
                usuarioMail.ape_Usuario = usuario.ape_Usuario;
                usuarioMail.email_Usr = usuario.email_Usr;
                usuarioMail.password = usuarioEnvia.password;
                Utility.Mail mail = new Mail();
                Task respuestaMail = mail.SendEmailAsync("Envío de clave provisoria", 2, null, null, usuarioMail);


                TempData["Mensaje"] = "Clave provisoria enviada a su correo electrónico.";
                return RedirectToAction("Index", "Home");
            } 
        }

        [HttpPost]
        public async Task<ActionResult> Modifica(string password_nueva, string password_nueva_repite)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response;
            Usuario usuarioEnvia = new Usuario();

            if(!(Session["user_Id"] == null) && Session["user_Id"].ToString() != "")
            {
                try
                {
                    usuarioEnvia.id_Usuario = Convert.ToDecimal(Session["user_Id"].ToString());
                    usuarioEnvia.cod_Estado_Usr = "VIG";
                    usuarioEnvia.password = password_nueva;
                    response = await client.PutAsJsonAsync(Resource.UsuarioAPIUrl, usuarioEnvia);

                    TempData["Cod"] = "OK";
                    TempData["Mensaje"] = "Cambio de clave realizado con exito!!!";
                    return RedirectToAction("Index", "CambioClave");

                }
                catch (Exception ex)
                {
                    TempData["Cod"] = "NO-OK";
                    TempData["Mensaje"] = ex.Message;
                    return RedirectToAction("Index", "CambioClave");
                }
              
            }
            return RedirectToAction("Index", "CambioClave");

        }

        private string generaClaveProvisoria()
        {
            
            var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var Charsarr = new char[8];
            var random = new Random();

            for (int i = 0; i < Charsarr.Length; i++)
            {
                Charsarr[i] = characters[random.Next(characters.Length)];
            }
            string resultString = new String(Charsarr);
            return resultString;
        }
    }
}