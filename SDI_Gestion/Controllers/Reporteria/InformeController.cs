﻿using Newtonsoft.Json;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SDI_Gestion.Controllers.Reporteria
{
    public class InformeController : BaseController
    {
		List<Object> myModel = new List<object>();
		// GET: Informe
		public ActionResult Index()
        {
			List<DataPoint> dataPoints = new List<DataPoint>();
			List<SDI_Gestion.Models.InformeHH> infoHoras = new List<SDI_Gestion.Models.InformeHH>();


			//Cargamos los meses
			InformeFiltro informeFiltro = new InformeFiltro();
			List<Mes> meses = new List<Mes>();
			List<Ano> anos = new List<Ano>();
			cargaMeses(meses);
			cargaAnos(anos);

			myModel.Add(informeFiltro);
			myModel.Add(meses);
			myModel.Add(anos);
			myModel.Add(infoHoras);
			ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
			return View(myModel);
		}

		// GET: Informe
		public async Task<ActionResult> Cargar(InformeFiltro informeFiltro)
		{

			var httpClient = new HttpClient();
			List<SDI_Gestion.Models.GraficoHH> infoGrafic = new List<SDI_Gestion.Models.GraficoHH>();
			List<SDI_Gestion.Models.InformeHH> infoHoras = new List<SDI_Gestion.Models.InformeHH>();

			//Rescatamos el usuario que esta conectado
			var json = await httpClient.GetStringAsync(Resource.GraficoHHAPIUrl + Session["user_Area"] +"/" + informeFiltro.mes + "/" + informeFiltro.ano);
			infoGrafic = JsonConvert.DeserializeObject<List<GraficoHH>>(json);

			json = await httpClient.GetStringAsync(Resource.InformeHHAPIUrl + Session["user_Area"] +"/" + informeFiltro.mes + "/" + informeFiltro.ano);
			infoHoras = JsonConvert.DeserializeObject<List<InformeHH>>(json);


			List<DataPoint> dataPoints = new List<DataPoint>();
			foreach(var dato in infoGrafic)
            {
				DataPoint dp = new DataPoint(dato.nombre_usuario, dato.total_Horas);
				dataPoints.Add(dp);
            }
			
			
			//Cargamos los meses
			List<Mes> meses = new List<Mes>();
			List<Ano> anos = new List<Ano>();
			cargaMeses(meses);
			cargaAnos(anos);

			myModel.Add(informeFiltro);
			myModel.Add(meses);
			myModel.Add(anos);
			myModel.Add(infoHoras);
			ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
			return View(myModel);
		}

		[HttpGet]
		public async Task<ActionResult> Exportar(int idArea, int ano, int mes)
		{
			var httpClient = new HttpClient();

			var respuesta = await httpClient.GetAsync($"{Resource.InformeHHAPIUrl}Exportar/{idArea}/{mes}/{ano}"); 
			var json = await respuesta.Content.ReadAsStringAsync();

			JsonResult jResult = Json(json, JsonRequestBehavior.AllowGet);
			jResult.MaxJsonLength = int.MaxValue;

			return jResult;
		}

		public void cargaMeses(List<Mes> meses)
		{
			meses.Add(new Mes { Id = 1, Descripcion = "Enero" });
			meses.Add(new Mes { Id = 2, Descripcion = "Febrero" });
			meses.Add(new Mes { Id = 3, Descripcion = "Marzo" });
			meses.Add(new Mes { Id = 4, Descripcion = "Abril" });
			meses.Add(new Mes { Id = 5, Descripcion = "Mayo" });
			meses.Add(new Mes { Id = 6, Descripcion = "Junio" });
			meses.Add(new Mes { Id = 7, Descripcion = "Julio" });
			meses.Add(new Mes { Id = 8, Descripcion = "Agosto" });
			meses.Add(new Mes { Id = 9, Descripcion = "Septiembre" });
			meses.Add(new Mes { Id = 10, Descripcion = "Octubre" });
			meses.Add(new Mes { Id = 11, Descripcion = "Noviembre" });
			meses.Add(new Mes { Id = 12, Descripcion = "Diciembre" });

		}

		public void cargaAnos(List<Ano> anos)
		{
			DateTime fechaActual = DateTime.Today;
			for (int inicio = fechaActual.Year; inicio >= fechaActual.Year -5 ; inicio--)
			{
				anos.Add(new Ano { Id = inicio, Descripcion = inicio.ToString() });
			}

		}

	}
}