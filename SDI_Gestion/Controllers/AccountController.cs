﻿using SDI_Gestion.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using Newtonsoft.Json;
using Microsoft.Graph;
using System.Web.Security;

namespace SDI_Gestion.Controllers
{
    public class AccountController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult> SignIn(string email, string password)
        {
            var httpClient = new HttpClient();
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
            {
                //Realizamos la llamada a la API para validar el usuario
                //Rescatamos el usuario que esta conectado
                Usuario usuario = new Usuario();
                var json = await httpClient.GetStringAsync(Resource.UsuarioLogin + email + "/" +  password);
                EstatusLogin estadoLogin = JsonConvert.DeserializeObject<EstatusLogin>(json);


                if (estadoLogin.Codigo.Equals("ok"))
                {
                    json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + email);
                    usuario = JsonConvert.DeserializeObject<Usuario>(json);
                    if (!string.IsNullOrEmpty(usuario.nombre_Usr))
                    {
                        //FormsAuthentication.SetAuthCookie(usuario.email_Usr, true);
                        //FormsAuthentication.SetAuthCookie(usuario.nombre_Usr, true);
                        //FormsAuthentication.SetAuthCookie(usuario.id_Usuario.ToString(), true);
                       
                        Session["user_Estado"] = usuario.cod_Estado_Usr;

                        if (usuario.cod_Estado_Usr.Equals("PEN"))
                        {
                            Session["user_Id"] = usuario.id_Usuario;
                            return RedirectToAction("Index", "CambioClave");
                        }
                        else
                        {
                            Session["user_Name"] = usuario.nombre_Usr;
                            Session["user_Apellido"] = usuario.ape_Usuario;
                            Session["user_Id"] = usuario.id_Usuario;
                            Session["user_Email"] = usuario.email_Usr;
                            Session["user_Area"] = usuario.id_Area;
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else 
                    {
                        TempData["Mensaje"] = "Error al autenticar usuario.";
                        return RedirectToAction("Index", "Home");
                    }
                } else
                {
                    TempData["Mensaje"] = estadoLogin.Descripcion;
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                TempData["Mensaje"] = "Error al autenticar usuario.";
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult SignOut()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}