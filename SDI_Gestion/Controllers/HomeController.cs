﻿using SDI_Gestion.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Threading.Tasks;
using SDI_Gestion.Models;
using SDI_Gestion.Utility;
using Newtonsoft.Json;
using Microsoft.Graph;
using System.IO;

namespace SDI_Gestion.Controllers
{
    public class HomeController : BaseController
    {
        List<Object> myModel = new List<object>();
        List<Movimiento> movimientosSolicitud;
        public async Task<ActionResult> Index()
        {
            var httpClient = new HttpClient();
            List<Movimiento> movimientos = new List<Movimiento>();
            EstadoSolicitudMovimiento estadoMovimiento = new EstadoSolicitudMovimiento();
            SDI_Gestion.Models.Solicitud solicitudAsigna = new SDI_Gestion.Models.Solicitud();

            if (!(Session["user_Name"] == null))
            {
                //Rescatamos las solicitudes que estan asignadas al usuario pendientes de realizar
                var json = await httpClient.GetStringAsync(Resource.BandejaSolicitudesUsuarioAPIUrl + Session["user_Id"].ToString().Trim());
                movimientos = JsonConvert.DeserializeObject<List<Movimiento>>(json);
            }


            foreach (var mov in movimientos)
            {
                movimientosSolicitud = new List<Movimiento>();
                var json = await httpClient.GetStringAsync(Resource.MovimientoSolicitudAPIUrl + mov.id_Solicitud);
                movimientosSolicitud = JsonConvert.DeserializeObject<List<SDI_Gestion.Models.Movimiento>>(json);
                mov.solicitud_Movimiento.movimientos_Solicitud = movimientosSolicitud;
            }

            myModel.Add(movimientos);
            myModel.Add(estadoMovimiento);
            myModel.Add(solicitudAsigna);
            ViewBag.Message2 = "Home";
            return View(myModel);
        }

        [HttpPost]
        public async Task<ActionResult> Iniciacion(SDI_Gestion.Models.Solicitud solicitud, SDI_Gestion.Models.EstadoSolicitudMovimiento estadoSolicitudMovimiento)
        {
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitudActualizar = new SDI_Gestion.Models.Solicitud();
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;


            if (estadoSolicitudMovimiento.id_Usuario.Equals(0))
            {
                //Rescatamos el usuario que esta conectado
                var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
                Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);
                estadoSolicitudMovimiento.id_Usuario = usuario.id_Usuario;
            }

            var json2 = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + solicitud.id_Solicitud);
            solicitudActualizar = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json2);


            solicitudActualizar.cod_Estado_Sol = "PRC";
            solicitudActualizar.fch_Estado_Sol = fechaActual;
            //ACTUALIZAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitudActualizar);


            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimiento.id_Solicitud = solicitud.id_Solicitud;
            estadoSolicitudMovimiento.horas_Dedicadas = 0;
            estadoSolicitudMovimiento.fch_Movimiento = fechaActual;
            estadoSolicitudMovimiento.Cod_Estado_Sol = "PRC";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimiento);

            //TempData["Mensaje"] = "Asigación de solicitud correcta";

            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        public async Task<ActionResult> Resolucion(SDI_Gestion.Models.Solicitud solicitudResolucion, SDI_Gestion.Models.EstadoSolicitudMovimiento estadoSolicitudMovimientoResolucion, SDI_Gestion.Models.Archivo archivo)
        {
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitudActualizar = new SDI_Gestion.Models.Solicitud();
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;


            if (estadoSolicitudMovimientoResolucion.id_Usuario.Equals(0))
            {
                //Rescatamos el usuario que esta conectado
                var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
                Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);
                estadoSolicitudMovimientoResolucion.id_Usuario = usuario.id_Usuario;
            }

            var json2 = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + solicitudResolucion.id_Solicitud);
            solicitudActualizar = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json2);


            solicitudActualizar.cod_Estado_Sol = "RES";
            solicitudActualizar.fch_Estado_Sol = fechaActual;
            //ACTUALIZAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitudActualizar);


            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimientoResolucion.id_Solicitud = solicitudResolucion.id_Solicitud;
            estadoSolicitudMovimientoResolucion.fch_Movimiento = fechaActual;
            estadoSolicitudMovimientoResolucion.Cod_Estado_Sol = "RES";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimientoResolucion);

            Movimiento movimientoGenerado = JsonConvert.DeserializeObject<SDI_Gestion.Models.Movimiento>(await respuesta.Content.ReadAsStringAsync());
            if (!(archivo.ImageFile == null))
            {
                //genera algunos datos de Documento
                Documento documento = new Documento();
                string evidencia;
                string extension = Path.GetExtension(archivo.ImageFile.FileName);
                string formato = (archivo.ImageFile.ContentType);
                string nombreArchivo = Path.GetFileNameWithoutExtension(archivo.ImageFile.FileName);
                using (var binaryreader = new BinaryReader(archivo.ImageFile.InputStream))
                {
                    evidencia = Convert.ToBase64String(binaryreader.ReadBytes(archivo.ImageFile.ContentLength));
                }
                //Enviamos el documento
                documento.idMovimiento = movimientoGenerado.id_Movimiento;
                documento.extencion = extension;
                documento.evidencia = evidencia;
                documento.formato = formato;
                documento.nombreDocumento = nombreArchivo;
                respuesta = await httpClient.PostAsJsonAsync(Resource.DocumentoAPIUrl, documento);
            }

            return RedirectToAction("Index", "Home");

        }

        [HttpPost]
        public async Task<ActionResult> Espera(SDI_Gestion.Models.Solicitud solicitudEspera, SDI_Gestion.Models.EstadoSolicitudMovimiento estadoSolicitudMovimientoEspera)
        {
            var httpClient = new HttpClient();
            SDI_Gestion.Models.Solicitud solicitudActualizar = new SDI_Gestion.Models.Solicitud();
            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0);
            HttpResponseMessage respuesta;


            if (estadoSolicitudMovimientoEspera.id_Usuario.Equals(0))
            {
                //Rescatamos el usuario que esta conectado
                var json = await httpClient.GetStringAsync(Resource.UsuarioMailAPIUrl + Session["user_Email"].ToString().Trim());
                Usuario usuario = JsonConvert.DeserializeObject<Usuario>(json);
                estadoSolicitudMovimientoEspera.id_Usuario = usuario.id_Usuario;
            }

            var json2 = await httpClient.GetStringAsync(Resource.SolicitudAPIUrl + solicitudEspera.id_Solicitud);
            solicitudActualizar = JsonConvert.DeserializeObject<SDI_Gestion.Models.Solicitud>(json2);


            solicitudActualizar.cod_Estado_Sol = "ESP";
            solicitudActualizar.fch_Estado_Sol = fechaActual;
            //ACTUALIZAMOS LA SOLCITUD
            respuesta = await httpClient.PutAsJsonAsync(Resource.SolicitudAPIUrl, solicitudActualizar);


            //INSERTAMOS UN NUEVO ESTADO DE SOLICITUD
            //GENEREAMOS EL ESTADO ASOCIADO AL INGRESO
            estadoSolicitudMovimientoEspera.id_Solicitud = solicitudEspera.id_Solicitud;
            estadoSolicitudMovimientoEspera.horas_Dedicadas = 0;
            estadoSolicitudMovimientoEspera.fch_Movimiento = fechaActual;
            estadoSolicitudMovimientoEspera.Cod_Estado_Sol = "ESP";
            respuesta = await httpClient.PostAsJsonAsync(Resource.MovimientoAPIUrl, estadoSolicitudMovimientoEspera);

            //TempData["Mensaje"] = "Asigación de solicitud correcta";

            return RedirectToAction("Index", "Home");

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}